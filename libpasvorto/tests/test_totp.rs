#[cfg(test)]
mod test_totp {
    use libpasvorto::{
        hash::{hash_sha1, hash_sha2_256, hash_sha2_512},
        tools::b32decode_rfc4648,
        totp::generate,
    };

    #[test]
    fn test_totp_sha1() {
        let test_key = b32decode_rfc4648(b"gezdgnbvgy3tqojqgezdgnbvgy3tqojq").unwrap();
        assert_eq!(20, test_key.len());

        let res = generate(hash_sha1, &test_key, 59u64, 30, 8).unwrap();
        assert_eq!("94287082", res.otp);

        let res = generate(hash_sha1, &test_key, 1111111109u64, 30, 8).unwrap();
        assert_eq!("07081804", res.otp);

        let res = generate(hash_sha1, &test_key, 1111111111u64, 30, 8).unwrap();
        assert_eq!("14050471", res.otp);

        let res = generate(hash_sha1, &test_key, 1234567890u64, 30, 8).unwrap();
        assert_eq!("89005924", res.otp);

        let res = generate(hash_sha1, &test_key, 2000000000u64, 30, 8).unwrap();
        assert_eq!("69279037", res.otp);

        let res = generate(hash_sha1, &test_key, 20000000000u64, 30, 8).unwrap();
        assert_eq!("65353130", res.otp);
    }

    #[test]
    fn test_totp_sha2_256() {
        let test_key =
            b32decode_rfc4648(b"gezdgnbvgy3tqojqgezdgnbvgy3tqojqgezdgnbvgy3tqojqgeza====").unwrap();
        assert_eq!(32, test_key.len());

        let res = generate(hash_sha2_256, &test_key, 59u64, 30, 8).unwrap();
        assert_eq!("46119246", res.otp);

        let res = generate(hash_sha2_256, &test_key, 1111111109u64, 30, 8).unwrap();
        assert_eq!("68084774", res.otp);

        let res = generate(hash_sha2_256, &test_key, 1111111111u64, 30, 8).unwrap();
        assert_eq!("67062674", res.otp);

        let res = generate(hash_sha2_256, &test_key, 1234567890u64, 30, 8).unwrap();
        assert_eq!("91819424", res.otp);

        let res = generate(hash_sha2_256, &test_key, 2000000000u64, 30, 8).unwrap();
        assert_eq!("90698825", res.otp);

        let res = generate(hash_sha2_256, &test_key, 20000000000u64, 30, 8).unwrap();
        assert_eq!("77737706", res.otp);
    }

    #[test]
    fn test_totp_sha2_512() {
        let test_key = b32decode_rfc4648(b"GEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQGEZDGNA").unwrap();
        assert_eq!(64, test_key.len());

        let res = generate(hash_sha2_512, &test_key, 59u64, 30, 8).unwrap();
        assert_eq!("90693936", res.otp);

        let res = generate(hash_sha2_512, &test_key, 1111111109u64, 30, 8).unwrap();
        assert_eq!("25091201", res.otp);

        let res = generate(hash_sha2_512, &test_key, 1111111111u64, 30, 8).unwrap();
        assert_eq!("99943326", res.otp);

        let res = generate(hash_sha2_512, &test_key, 1234567890u64, 30, 8).unwrap();
        assert_eq!("93441116", res.otp);

        let res = generate(hash_sha2_512, &test_key, 2000000000u64, 30, 8).unwrap();
        assert_eq!("38618901", res.otp);

        let res = generate(hash_sha2_512, &test_key, 20000000000u64, 30, 8).unwrap();
        assert_eq!("47863826", res.otp);
    }
}
