#[macro_export]
macro_rules! dyn_trunc {
    ($input: expr) => {
        crate::tools::dynamic_truncation(
            $input,
            crate::tools::SLICE_SIZE,
            crate::tools::OUTPUT_MASK,
        )
    };
    ($input: expr, $slice_size:expr) => {
        crate::tools::dynamic_truncation($input, $slice_size, crate::tools::OUTPUT_MASK)
    };
}
