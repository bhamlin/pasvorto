use chrono::Utc;

use crate::{
    dyn_trunc,
    hash::{hash_sha1, hash_sha2_256, hash_sha2_512},
};

pub struct TOTPResult {
    pub otp: String,
    pub life: u8,
}

pub fn get_last_n(input: String, count: usize) -> String {
    input
        .chars()
        .rev()
        .take(count)
        .collect::<String>()
        .chars()
        .rev()
        .collect()
}

#[allow(unused)]
pub fn generate<F>(hasher: F, key: &[u8], time: u64, step: u8, length: u8) -> Option<TOTPResult>
where
    F: Fn(&[u8], &[u8]) -> Vec<u8>,
{
    let life = time % 30;
    let life = 30 - (life as u8);
    let now = u64::to_be_bytes(time / 30);

    let otp = dyn_trunc!(&hasher(key, &now))?;

    let otp = otp % 10_u32.pow(length as u32);
    let otp = format!("{otp:016}");
    let otp: String = get_last_n(otp, length.into());

    Some(TOTPResult { otp, life })
}

pub fn totp_sha1(key: &[u8]) -> Option<TOTPResult> {
    generate(hash_sha1, key, Utc::now().timestamp() as u64, 30, 6)
}

pub fn totp_sha2_256(key: &[u8]) -> Option<TOTPResult> {
    generate(hash_sha2_256, key, Utc::now().timestamp() as u64, 30, 6)
}

pub fn totp_sha2_512(key: &[u8]) -> Option<TOTPResult> {
    generate(hash_sha2_512, key, Utc::now().timestamp() as u64, 30, 6)
}
