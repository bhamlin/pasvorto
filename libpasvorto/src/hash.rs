use hmac::{Hmac, Mac};
use sha1::Sha1;
use sha2::{Sha256, Sha512};

pub fn hash_sha1(key: &[u8], time: &[u8]) -> Vec<u8> {
    let mut h = Hmac::<Sha1>::new_from_slice(key).ok().unwrap();
    h.update(time);
    let output = h.finalize().into_bytes();
    output.to_vec()
}

pub fn hash_sha2_256(key: &[u8], time: &[u8]) -> Vec<u8> {
    let mut h = Hmac::<Sha256>::new_from_slice(key).ok().unwrap();
    h.update(time);
    let output = h.finalize().into_bytes();
    output.to_vec()
}

pub fn hash_sha2_512(key: &[u8], time: &[u8]) -> Vec<u8> {
    let mut h = Hmac::<Sha512>::new_from_slice(key).ok().unwrap();
    h.update(time);
    let output = h.finalize().into_bytes();
    output.to_vec()
}
