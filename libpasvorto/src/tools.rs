pub mod base32;

pub const SLICE_SIZE: usize = 4;
pub const OUTPUT_MASK: u32 = 0x7fff_ffff;

pub fn dynamic_truncation(input: &[u8], slice_size: usize, output_mask: u32) -> Option<u32> {
    let length = input.len();
    let idx = input[length - 1] & 0xf;
    let idx = idx as usize;
    let idn = idx + slice_size;
    let slice: [u8; 4] = input[idx..idn].try_into().ok()?;
    let value_raw = u32::from_be_bytes(slice);
    let value = value_raw & output_mask;

    Some(value)
}

pub fn b32decode_rfc4648(data: &[u8]) -> Option<Vec<u8>> {
    base32::decode(base32::RFC4648_INV_ALPHABET, data)
}
